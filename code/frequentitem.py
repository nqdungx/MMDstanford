import numpy as np


b1 = ('B','P')
b2 = ('C','M')
b3 = ('B','C','J')
b4 = ('P','M')
b5 = ('B','C','M')
b6 = ('M','J')
b7 = ('P','J')
b8 = ('B','C','M','J')

itemset = ('B','C','P','M','J')

buckets = [b1,b2,b3,b4,b5,b6,b7,b8]

for i in range(len(itemset)):
    for j in range(i+1,len(itemset),1):
        count = 0
        for b in buckets:
            if itemset[i] in b and itemset[j] in b:
                count+=1
        print itemset[i],itemset[j],count



timestamp = np.array(range(-1,75,1))
value = timestamp % 10+1
value = value[1:]

#value = ['a', 'b', 'c', 'b', 'd', 'a', 'c', 'd', 'a', 'b', 'd', 'c', 'a', 'a','b']
dic = {}
for i in set(value):
    for j in value:
        if i==j:
            if i not in dic:
                dic[i] = 1
            else:
                dic[i]+=1
v =  np.array(dic.values())
sumprimeNumber = v.dot(v)



def ams(value, picks, n):
    dic = {}
    for p in picks:
        item = value[p - 1]
        for i in range(p-1, len(value),1):
            if item == value[i]:
                if item not in dic:
                    dic[item] = 1
                else:
                    dic[item]+=1
    for k in dic:
        dic[k] = n*(2*dic[k]-1)

    return np.average(dic.values())

print abs(sumprimeNumber - ams(value,(31,32,44), 75))
print abs(sumprimeNumber - ams(value,(3,45,72), 75))
print abs(sumprimeNumber - ams(value,(37,46,55), 75))
print abs(sumprimeNumber - ams(value,(14,35,42), 75))


def lofTail(num):

    binary = list("{0:b}".format(num))
    count = 0
    for i in range(len(binary)-1, 0, -1):
        if binary[i]=='1':
            break
        count+=1
    return count

elements =[map(lambda x: (3*x + 7)%11, i) for i in  [(2,5,7,10),(3,7,8,10),(1,4,7,9),(4,5,6,10)]] #[(4,5,6,7),(1,6,7,10),(1,2,3,9),(1,4,7,9)]]
print elements
for element in elements:
    distiction = map(lambda x: lofTail(x), element)
    print distiction


