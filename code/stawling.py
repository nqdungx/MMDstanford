import numpy as np

graph = {'A': ('B', 'C', 'E'),
         'B': ('A','C','E','D'),
         'C': ('A','B','E'),
         'D': ('B','E'),
         'E': ('A','B','C','D')}

itemsets = graph.keys()

import  itertools

def isCompletedGraph(x,y,graph):
    isCompleted = True
    for i in x:
        if isCompleted == False:
            return isCompleted
        else:
            for j in y:
                if j  not in graph[i]:
                    isCompleted = False
                    break
    return isCompleted

def stawling(x,y,graph):

    xcases = list(itertools.combinations([key for key in graph if len(graph[key])>=y], x))
    ycases = list(itertools.combinations([key for key in graph if len(graph[key])>=x], y))
    okList = []
    for x in xcases:
        for y in ycases:
            if x!=y:
                if len(x) == len(y) and ((x,y) in okList  or (y,x) in okList):
                    continue

                elif isCompletedGraph(x, y, graph):
                    okList.append((x, y))

    return len(set(okList))



print stawling(2,2,graph)


